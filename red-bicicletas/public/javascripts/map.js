mapboxgl.accessToken = 'pk.eyJ1IjoiY29udHJhdGlzdGF3ZWIiLCJhIjoiY2tmMGozcHR0MHo3MTJyczJndnYyZ2tvbSJ9.x8v59-tRnikz3HHXmlnE4A';

var map = new mapboxgl.Map({
    container: 'map',
    // style: 'mapbox://styles/mapbox/streets-v11'
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [-75.5572536, 6.2686913],
    zoom: 2,
})

map.addControl(new mapboxgl.NavigationControl());
map.addControl(new mapboxgl.FullscreenControl());

map.on("load", function () {
    $.ajax({
        dataType: "json",
        url: "api/bicicletas",
        success: function (result) {
            result.bicicletas.forEach(function (bici) {
                console.log(`bicicleta: ${bici.id}`);
                const element = document.createElement('div');
                element.className = 'marker';
                const marker = new mapboxgl.Marker(element)
                    .setLngLat({
                        lng: bici.ubicacion[1],
                        lat: bici.ubicacion[0]
                    })
                    .addTo(map);
            })
        }
    })
})