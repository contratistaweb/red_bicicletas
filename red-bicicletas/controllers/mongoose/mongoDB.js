var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // estamos conectados!
});

var ciudadanoSchema = new mongoose.Schema({
    nombre: String,
    edad: Number
});

var Ciudadano = mongoose.model('Ciudadano', ciudadanoSchema);

var unCiudadano = new Ciudadano({
    nombre: 'Pedro',
    edad: 21
});
console.log(unCiudadano.nombre); // 'Pedro'

ciudadanoSchema.methods.saludar = function () {
    var saludo = this.nombre ?
        "Hola, mi nombre es " + this.name :
        "Hola, no tengo nombre";
    console.log(saludo);
}
var Ciudadano = mongoose.model('Ciudadano', ciudadanoSchema);

var unCiudadano = new Ciudadano({
    nombre: 'Pedro',
    edad: 21
});
unCiudadano.saludar(); // "Hola, mi nombre es Pedro"

unCiudadano.save(function (err, unCiudadano) {
    if (err) return console.error(err);
    unCiudadano.saludar();
});

Ciudadano.find(function (err, ciudadanos) {
    if (err) return console.error(err);
    console.log(ciudadanos);
})

Ciudadano.find({ nombre: /^Pedro/ }, callback); 