const mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../models/bicicleta');
var request = require('request');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost:27017/red_bicicletas';
        mongoose.connect(mongoDB);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database!');
        });

    })

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        })
    })

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (err, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            })
        });
    });

    // describe("POST BICICLETAS /create", () => {
    //     it("Status 200", (done) => {
    //         var headers = {
    //             'content-type': 'application/json'
    //         };
    //         var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -51}';
    //         request.post({
    //             headers: headers,
    //             url: base_url,
    //             body: aBici
    //         }, function (error, response, body) {
    //             expect(response.statusCode).toBe(200);
    //             var bici = JSON.parse(body).bicicleta;
    //             console.log(bici);
    //             expect(bici.color).toBe("rojo");
    //             expect(bici.ubicacion[0]).toBe(-34);
    //             expect(bici.ubicacion[1]).toBe(-51);
    //             done();
    //         });
    //     });
    // });


});

// describe('POST BICICLETAS /create', () => {
//     it('STATUS 200', (done) => {
//         var headers = {'content-type' : 'application/json'};
//         var aBici = '{ "id" : 10, "color" : "rojo", "modelo" : "urbana", "lat" : -34, "lng" : -54 }';
//         request.post({
//             headers: headers,
//             url: 'http://localhost:3000/api/bicicletas/create',
//             body: aBici
//         }, (error, response, body) => {
//             expect(response.statusCode).toBe(200);
//             expect(Bicicleta.findById(10).color).toBe("rojo");
//             done();
//         });
//     });
// });